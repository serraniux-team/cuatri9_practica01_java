package com.example.holamundo_01_java;
import androidx.appcompat.app.AppCompatActivity;
import  android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends  AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void  saludar(View v){
        EditText nombreUser=findViewById(R.id.txtNombre);
        Toast.makeText(this, "Hola "+nombreUser.getText().toString() ,Toast.LENGTH_LONG).show();
    }
}
